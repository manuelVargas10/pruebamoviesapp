class ResponseDB {
  bool isSuccess;
  String message;
  dynamic result;

  ResponseDB({
    this.isSuccess = false,
    this.message = '',
    this.result,
  });

  dynamic get response {
    return result;
  }
}