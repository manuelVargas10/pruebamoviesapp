class MovieModel {
  MovieModel({
    this.title = '',
    this.rank = '',
    this.id = '',
    this.image = ''
  });

  String title;
  String rank;
  String id;
  String image;


  factory MovieModel.fromJson(Map<String, dynamic> json) => MovieModel(
    title: json["title"],
    rank: json["rank"],
    id: json["id"],
    image: json['image']??'https://ep01.epimg.net/cultura/imagenes/2019/07/22/actualidad/1563791717_146941_1563803892_noticia_normal.jpg'
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "rank": rank,
    "id": id,
    "image": image,
  };
}
