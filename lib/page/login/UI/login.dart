import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/utils/validation.dart';
import 'package:pruebas/widget/buttonWidget.dart';
import 'package:pruebas/widget/inputWidget.dart';
import 'package:pruebas/widget/titleApp.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late TextEditingController userController;
  late TextEditingController passwordController;
  late Size size;
  late final GlobalKey<FormState> _formKey;

  @override
  void initState() {
    userController = TextEditingController();
    passwordController = TextEditingController();
    _formKey = GlobalKey<FormState>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.blueGrey[700],
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        height: size.height,
        width: size.width,
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TitleApp(),
              SizedBox(height: 50),
              InputWidget(
                controller: userController,
                labelInput: 'Correo',
                iconData: Icons.person,
                validation:(e){
                  if(e!.isEmpty) return 'este campo no puede ser vacio';
                  else if(!emailValidation(e)) return 'formato incorrecto';
                  else return null;
                }
              ),
              SizedBox(height: 10),
              InputWidget(
                controller: userController,
                labelInput: 'Contrasena',
                iconData: Icons.vpn_key_outlined,
                validation:(e){
                  if(e!.isEmpty) return 'este campo no puede ser vacio';
                  else return null;
                }
              ),
              SizedBox(height: 50),
              ButtonWidget(
                label: 'iniciar',
                handledButton: handledInit,
              )
            ],
          ),
        ),
      ),
    );
  }


  handledInit(){
    if (_formKey.currentState!.validate()) {
      Navigator.of(context).pushReplacementNamed('/home');
    }
  }
}
