import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/model/movieModel.dart';
import 'package:pruebas/widget/ratingWidget.dart';

class HeaderCollapse extends StatelessWidget {
  final Widget body;
  final MovieModel movie;
  late Size size;
  final double rating;
  final Function(double) handledRating;

  HeaderCollapse({required this.body, required this.movie, required this.rating, required this.handledRating});

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return DefaultTabController(
        length: 2,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: Colors.blueGrey[900],
                expandedHeight: 250.0,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                    background: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Image.network(
                          movie.image,
                          fit: BoxFit.cover,
                        ),
                        Center(
                          child: ClipRect(
                            // <-- clips to the 200x200 [Container] below
                            child: BackdropFilter(
                              filter: ImageFilter.blur(
                                sigmaX: 2.0,
                                sigmaY: 2.0,
                              ),
                              child: Container(
                                color: Colors.black54,
                                alignment: Alignment.center,
                                width: size.width,
                                height: size.height,
                                child: Container(
                                  padding: EdgeInsets.only(top: 150, left: 10),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Hero(
                                          tag: movie.id,
                                          child: Container(
                                            child: Image(
                                              fit: BoxFit.cover,
                                              image: NetworkImage(movie.image),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                          flex: 3,
                                          child: Container(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Container(
                                                  alignment: Alignment.centerLeft,
                                                  child: Text(
                                                    movie.title,
                                                    style: TextStyle(
                                                        fontSize: 20.0,
                                                        color: Colors.white,
                                                        fontWeight:
                                                        FontWeight.w500),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Text(
                                                      '#${movie.rank}',
                                                      style: TextStyle(
                                                          fontSize: 20.0,
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                    SizedBox(width: 10),
                                                    Expanded(
                                                      child:
                                                      RatingWidget(handledRating: handledRating, rating: rating, size: 18,),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
            ];
          },
          body: Center(
            child: body,
          ),
        ));
  }
}
