import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/model/movieModel.dart';

class CardBodyList extends StatelessWidget {
  final MovieModel item;
  final Function(MovieModel) handledMovie;

  CardBodyList({required this.item, required this.handledMovie});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        handledMovie(item);
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
        child: Row(
          children: [
            Expanded(
              flex: 5,
              child: Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: Image(
                  fit: BoxFit.cover,
                  image: NetworkImage(item.image),
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 6,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.title,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w500),
                      textAlign: TextAlign.left,
                    ),
                    Row(
                      children: [
                        Text(
                          '#${item.rank}',
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.white,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox(width: 10),
                        Text(
                          'Junio 23, 2010',
                          style: TextStyle(
                              fontSize: 11.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
