import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pruebas/model/movieModel.dart';
import 'package:pruebas/page/detail/Ui/widget/headerCollapse.dart';
import 'package:pruebas/page/detail/UI/widget/cardBodyList.dart';
import 'package:pruebas/provider/detailProvider.dart';
import 'package:pruebas/provider/homeProvider.dart';
import 'package:pruebas/widget/line.dart';
import 'package:pruebas/widget/ratingWidget.dart';
import 'package:pruebas/widget/titleWidget.dart';

class Detail extends StatefulWidget {
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  late Size size;

  @override
  void initState() {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    Provider.of<DetailProvider>(context, listen: false)
        .getDataRatingByMovie(homeProvider.moviesSelect);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    HomeProvider homeProvider = Provider.of<HomeProvider>(context);
    DetailProvider detailProvider = Provider.of<DetailProvider>(context);
    size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.blueGrey[700],
        body: HeaderCollapse(
          rating: detailProvider.ratingMovie,
          handledRating: handledRating,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 20),
                  TitleWidget(label: homeProvider.moviesSelect.title),
                  SizedBox(height: 10),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    child: Text(
                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum nulla eget eu sed non eu morbi. Enim ligula cursus senectus ut mauris, id. Aliquam, enim consectetur venenatis ac. Viverra nunc, pretium condimentum at.',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  SizedBox(height: 30),
                  Line(),
                  SizedBox(height: 10),
                  TitleWidget(
                    label: 'Rating',
                  ),
                  RatingWidget(
                      handledRating: handledRating,
                      rating: detailProvider.ratingMovie),
                  SizedBox(height: 30),
                  Line(),
                  SizedBox(height: 10),
                  TitleWidget(
                    label: 'Mas Peliculas',
                  ),
                  ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: homeProvider.listMovies.length,
                      itemBuilder: (BuildContext context, int index) => Column(
                            children: [
                              CardBodyList(
                                  item: homeProvider.listMovies[index],
                                  handledMovie: handledMovie),
                              Line(margin: 20)
                            ],
                          )),
                ],
              ),
            ),
          ),
          movie: homeProvider.moviesSelect,
        ));
  }

  handledMovie(MovieModel itemSelect) {
    Provider.of<HomeProvider>(context, listen: false).moviesSelect = itemSelect;
    Navigator.of(context).pushReplacementNamed('/detail');
  }

  handledRating(double data) {
    HomeProvider homeProvider = Provider.of<HomeProvider>(context, listen: false);
    Provider.of<DetailProvider>(context, listen: false).setDataRatingByMovie(data, homeProvider.moviesSelect);
  }
}
