import 'package:pruebas/model/movieModel.dart';
import 'package:pruebas/model/responseDb.dart';
import 'package:pruebas/utils/abstractApi.dart';

class HomeApi extends AbstractApi {
  Future<List<MovieModel>> getDataUser() async {
    final ResponseDB response = await readData(urlSpecific: "https://raw.githubusercontent.com/hjorturlarsen/IMDB-top-100/master/data/movies.json");
    if (response.isSuccess) {
      List listData = response.result;
      List<MovieModel> accountModel = listData.map((e) => MovieModel.fromJson(e)).toList();
      return accountModel;
    }
    return [];
  }
}
