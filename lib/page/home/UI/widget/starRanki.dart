import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StarRank extends StatelessWidget {
  final int rank;

  StarRank({this.rank = 0});

  @override
  Widget build(BuildContext context) {
    return Row(
        children: List.generate(
            5,
            (index) => Icon(
                  Icons.star,
                  size: 17,
                  color: (5 * (100 - rank))/100 <= index ? Colors.black54 : Colors.yellowAccent,
                )));
  }
}
