import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/model/movieModel.dart';

class CardBodyCarrousel extends StatelessWidget {
  final MovieModel movie;
  final Function(MovieModel) handledMovie;

  CardBodyCarrousel({required this.movie, required this.handledMovie});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => handledMovie(movie),
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                            fit: BoxFit.fill, image: NetworkImage(movie.image)),
                        boxShadow: [
                      BoxShadow(
                        color: Colors.black54,
                        offset: Offset(0.0, 1.0), //(x,y)
                        blurRadius: 5.0,
                      ),
                    ])),
              ),
              SizedBox(height: 20),
              Text(
                movie.title,
                style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              Row(
                children: [
                  Text(
                    '#${movie.rank}',
                    style: TextStyle(
                        fontSize: 11.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w300),
                  ),
                  SizedBox(width: 10),
                  Text(
                    'Junio 23, 2010',
                    style: TextStyle(
                        fontSize: 11.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w300),
                  ),
                ],
              )
            ],
          )),
    );
  }
}
