import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Carrousel extends StatelessWidget {
  final CarouselController controller;
  final List<Widget> body;

  Carrousel({required this.controller, required this.body});

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      items: body,
      carouselController: controller,
      options: CarouselOptions(
        autoPlay: true,
        aspectRatio: 1.3,
        autoPlayAnimationDuration: Duration(seconds: 1),
        autoPlayInterval: Duration(seconds: 10),
        viewportFraction: 0.5,
        enlargeCenterPage: true,
        enlargeStrategy: CenterPageEnlargeStrategy.height,
      ),
    );
  }
}
