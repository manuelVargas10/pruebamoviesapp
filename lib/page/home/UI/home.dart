import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:provider/provider.dart';
import 'package:pruebas/model/movieModel.dart';
import 'package:pruebas/page/home/UI/widget/cardBodyCarrousel.dart';
import 'package:pruebas/page/home/UI/widget/cardBodyList.dart';
import 'package:pruebas/page/home/UI/widget/carrousel.dart';
import 'package:pruebas/provider/homeProvider.dart';
import 'package:pruebas/widget/line.dart';
import 'package:pruebas/widget/titleWidget.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late Size size;
  CarouselController buttonCarouselController = CarouselController();

  @override
  void initState() {
    Provider.of<HomeProvider>(context, listen: false).getMovies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    HomeProvider homeProvider = Provider.of<HomeProvider>(context);
    size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.blueGrey[700],
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          title: Text('Peliculas'),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 30),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TitleWidget(label: 'Top #10')),
              homeProvider.listMovies.length == 0
                  ? CircularProgressIndicator()
                  : Carrousel(
                      body: List.generate(
                          10,
                          (index) => CardBodyCarrousel(
                              movie: homeProvider.listMovies[index],
                              handledMovie: handledMovie)),
                      controller: buttonCarouselController,
                    ),
              SizedBox(height: 30),
              Line(),
              SizedBox(height: 30),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TitleWidget(label: 'Mas Peliculas')),
              homeProvider.listMovies.length == 0
                  ? CircularProgressIndicator()
                  : ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: homeProvider.listMovies.length,
                      itemBuilder: (BuildContext context, int index) => Column(
                            children: [
                              CardBodyList(
                                  item: homeProvider.listMovies[index],
                                  handledMovie: handledMovie),
                              Line(margin: 20),
                            ],
                          )),
            ],
          ),
        ));
  }

  handledMovie(MovieModel itemSelect) {
    Provider.of<HomeProvider>(context, listen: false).moviesSelect = itemSelect;
    Navigator.of(context).pushNamed('/detail');
  }
}
