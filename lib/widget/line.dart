import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Line extends StatelessWidget {

  final double margin;

  Line({this.margin = 10.0});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 0.1,
      color: Colors.grey,
      margin: EdgeInsets.symmetric(horizontal: margin),
    );
  }
}
