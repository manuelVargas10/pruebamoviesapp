import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputWidget extends StatelessWidget {
  final TextEditingController controller;
  final String labelInput;
  final IconData iconData;
  final validation;

  InputWidget({
    required this.controller,
    this.labelInput = '',
    this.iconData = Icons.ac_unit,
    required this.validation
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: validation,
      decoration: InputDecoration(
          prefixIcon: Icon(
            iconData,
            color: Colors.white,
          ),
          fillColor: Colors.black.withOpacity(0.6),
          filled: true,
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(8.0),
            ),
            borderSide: new BorderSide(
              color: Colors.transparent,
              width: 1.0,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide: BorderSide(
              color: Colors.white,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide: BorderSide(
              color: Colors.white,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide: BorderSide(
              color: Colors.red,
            ),
          ),
          labelText: labelInput,
          labelStyle: new TextStyle(color: Colors.white, fontSize: 16.0)),
    );
  }
}
