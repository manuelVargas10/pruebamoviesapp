import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  late Size size;
  final String label;
  final Function handledButton;

  ButtonWidget({this.label = '', required this.handledButton});

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return InkWell(
      onTap:() => handledButton(),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15),
        width: size.width,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(30)),
        child: Center(
          child: Text(
            'Iniciar',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
          ),
        ),
      ),
    );
  }
}
