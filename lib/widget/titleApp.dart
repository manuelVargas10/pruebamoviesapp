import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TitleApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment:MainAxisAlignment.center ,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'App Movil',
            style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontStyle: FontStyle.italic),
          ),
          SizedBox(width: 10,),
          Icon(Icons.movie_filter_sharp, color: Colors.white, size: 40)

        ],
      ),
    );
  }
}
