import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/model/movieModel.dart';

class DetailProvider with ChangeNotifier {
  double _ratingMovie = 0.0;

  double get ratingMovie => _ratingMovie;

  set ratingMovie(double movies) {
    _ratingMovie = movies;
    notifyListeners();
  }

  Future<void> setDataRatingByMovie(double rating, MovieModel movie) async {
    await Firebase.initializeApp();
    FirebaseFirestore.instance
        .collection('rating')
        .doc(movie.id)
        .set({'id': movie.id, 'rating': rating});
    ratingMovie = rating;
  }

  Future<void> getDataRatingByMovie(MovieModel movie) async {
    await Firebase.initializeApp();
    final data = await FirebaseFirestore.instance
        .collection('rating')
        .doc(movie.id)
        .get();
    dynamic rating = data.data();
    ratingMovie = rating != null ? rating['rating'] : 0.0;
  }

  @override
  void dispose() {
    _ratingMovie = 0.0;
    super.dispose();
  }
}
