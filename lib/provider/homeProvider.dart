import 'package:flutter/material.dart';
import 'package:pruebas/model/movieModel.dart';
import 'package:pruebas/page/home/Api/homeApi.dart';

class HomeProvider with ChangeNotifier {
  List<MovieModel> _listMovies = [];
  var _moviesSelect;

  List<MovieModel> get listMovies => _listMovies;
  MovieModel get moviesSelect => _moviesSelect;

  set listMovies(List<MovieModel> movies) {
    _listMovies = movies;
    notifyListeners();
  }

  set moviesSelect(MovieModel movies) {
    _moviesSelect = movies;
    notifyListeners();
  }

  getMovies() async => listMovies = await HomeApi().getDataUser();
}
