import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pruebas/constants.dart';
import 'package:pruebas/page/login/UI/login.dart';
import 'package:pruebas/provider/detailProvider.dart';
import 'package:pruebas/provider/homeProvider.dart';
import 'package:pruebas/routes.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => HomeProvider()),
        ChangeNotifierProvider(create: (_) => DetailProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          scaffoldBackgroundColor: kScaffoldColor,
        ),
        routes: {
          '/': (BuildContext context) {
            return Login();
          },
          ...Routes.getRoutes(context: context)
        },
      ),
    );
  }
}