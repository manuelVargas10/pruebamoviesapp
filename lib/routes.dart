import 'package:flutter/material.dart';
import 'package:pruebas/page/detail/Ui/detail.dart';
import 'package:pruebas/page/home/UI/home.dart';
import 'package:pruebas/page/login/UI/login.dart';

class Routes {
  static getRoutes({required BuildContext context}) {
    return {
      '/': (BuildContext context) => Login(),
      '/home': (BuildContext context) => Home(),
      '/detail': (BuildContext context) => Detail(),
    };
  }
}


