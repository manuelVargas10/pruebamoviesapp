import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:pruebas/model/responseDb.dart';

class AbstractApi {


  Future<ResponseDB> readData({required String urlSpecific}) async {
    try{
      final response = await http.get(Uri.parse(urlSpecific));
      final dataDecode = response.body != "" ? json.decode(response.body) : [];
      if (response.statusCode >= 200 && response.statusCode <= 204) {
        return ResponseDB(
          isSuccess: true,
          message: 'Operacion realizada con exito',
          result: dataDecode,
        );
      } else {
        return ResponseDB(
          isSuccess: false,
          message: dataDecode['message'],
          result: [],
        );
      }
    }catch(error){
      return ResponseDB(
        isSuccess: false,
        message: 'error en la peticion',
        result: [],
      );
    }
  }

}