import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFF7D724);
const kSecondaryColor = Color(0xFF1A2146);
const kWhiteColor = Color(0xFFFFFFFF);
const kScaffoldColor = Color(0xFFF8FBFF);